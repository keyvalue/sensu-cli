package main

import (
	"fmt"
	"log"

	sensu "github.com/jefflaplante/sensulib"
)

func main() {

	config := sensu.DefaultConfig()
	config.Address = "sensu.paperlesspost.net:80"

	// Create a new API Client
	sensuAPI, err := sensu.NewAPIClient(config)
	if err != nil {
		log.Fatalf("could not make api client: %v", err)
		// catch errors in client creation
	}

	// Get All clients that Sensu API server knows about
	var clients []sensu.Client
	_, err = sensuAPI.GetClients(&clients)
	if err != nil {
		log.Fatalf("could not get clients: %v", err)
		// catch errors in client creation
	}

	fmt.Printf("First Client: %+v\n", clients[0])
	fmt.Printf("num clients: %v\n", len(clients))

	return

}
